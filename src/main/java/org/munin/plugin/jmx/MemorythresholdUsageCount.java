package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;

public final class MemorythresholdUsageCount extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemorythresholdUsageCount", "count",
                "Returns the number of times that the memory usage has "
                + "crossed the usage threshold."
        ).addItem(new GraphItemBuilder("TenuredGen")
                .setInfo("UsageThresholdCount for Tenured Gen.")
        ).addItem(new GraphItemBuilder("PermGen")
                .setInfo("UsageThresholdCount for Perm Gen."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GetUsageThresholdCount collector =
                new GetUsageThresholdCount(connection);
        final String[] temp = collector.GC();

        Output.write("TenuredGen.value %s", temp[0]);
        Output.write("PermGen.value %s", temp[1]);
    }
}
