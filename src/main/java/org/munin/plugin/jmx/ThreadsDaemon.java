package org.munin.plugin.jmx;
/**
 *
 * @author Diyar
 */

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class ThreadsDaemon extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ThreadsDaemon", "threads",
                "Returns the current number of live daemon threads."
        ).addItem(new GraphItemBuilder("ThreadsDaemon"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final ThreadMXBean threadmxbean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);

        Output.write("ThreadsDaemon.value %s",
                threadmxbean.getDaemonThreadCount());
    }
}
