package org.munin.plugin.jmx;

import org.nohope.rrdtool.DrawType;
import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

public final class MemoryAllocatedHeap extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemoryAllocatedHeap", "bytes", null
        ).setArgs("--base 1024 -l 0"
        ).addItem(new GraphItemBuilder("Max")
                .setDraw(DrawType.AREA)
                .setColour("ccff00")
                .setInfo("The maximum amount of memory (in bytes) that can be "
                          + "used for memory management.")
        ).addItem(new GraphItemBuilder("Committed")
                .setDraw(DrawType.LINE2)
                .setColour("0033ff")
                .setInfo("The amount of memory (in bytes) that is guaranteed "
                         + "to be available for use by the Java virtual "
                         + "machine.")
        ).addItem(new GraphItemBuilder("Init")
                .setInfo("The initial amount of memory (in bytes) that the "
                         + "Java virtual machine requests from the operating "
                         + "system for memory management during startup.")
        ).addItem(new GraphItemBuilder("Used")
                .setDraw(DrawType.LINE3)
                .setColour("33cc00")
                .setInfo("Represents the amount of memory currently used "
                         + "(in bytes)"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final MemoryMXBean mBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.MEMORY_MXBEAN_NAME,
                        MemoryMXBean.class);

        Output.write("Committed.value %s",
                mBean.getHeapMemoryUsage().getCommitted());
        Output.write("Max.value %s",
                mBean.getHeapMemoryUsage().getMax());
        Output.write("Init.value %s",
                mBean.getHeapMemoryUsage().getInit());
        Output.write("Used.value %s",
                mBean.getHeapMemoryUsage().getUsed());
    }
}
