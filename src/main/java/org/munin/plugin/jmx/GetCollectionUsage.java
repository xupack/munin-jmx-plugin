package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class GetCollectionUsage {
    private List<MemoryPoolMXBean> gcMBeans;
    private final String[] gcResult = new String[5];
    private final MBeanServerConnection connection;
    private final int memType;

    public GetCollectionUsage(final MBeanServerConnection connection, final int memType) {
        this.memType = memType;
        this.connection = connection;
    }

    public String[] GC() throws IOException, MalformedObjectNameException {
        final ObjectName gcName = new ObjectName(
                ManagementFactory.MEMORY_POOL_MXBEAN_DOMAIN_TYPE + ",*");


        final Set mBeans = connection.queryNames(gcName, null);
        if (mBeans != null) {
            gcMBeans = new ArrayList<MemoryPoolMXBean>();
            for (final Object mBean : mBeans) {
                final ObjectName objName = (ObjectName) mBean;
                final MemoryPoolMXBean gc = ManagementFactory.newPlatformMXBeanProxy(connection, objName
                        .getCanonicalName(),
                        MemoryPoolMXBean.class);
                gcMBeans.add(gc);
            }
        }


        int i = 0;
        final MemoryPoolMXBean mem = gcMBeans.get(memType);
        gcResult[i++] = Long.toString(mem.getCollectionUsage().getCommitted());
        gcResult[i++] = Long.toString(mem.getCollectionUsage().getInit());
        gcResult[i++] = Long.toString(mem.getCollectionUsage().getMax());
        gcResult[i++] = Long.toString(mem.getCollectionUsage().getUsed());
        gcResult[i] = Long.toString(mem.getCollectionUsageThreshold());
        return gcResult;
    }
}

