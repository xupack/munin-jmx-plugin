package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public final class Uptime extends EntryPoint {
    private static final double MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24;

    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM Uptime", "Days",
                "Uptime of the Java virtual machine in days."
        ).addItem(new GraphItemBuilder("Uptime"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final RuntimeMXBean osMXBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.RUNTIME_MXBEAN_NAME,
                        RuntimeMXBean.class);

        Output.write("Uptime.value %s",
                osMXBean.getUptime() / MILLISECONDS_PER_DAY);
    }

}
