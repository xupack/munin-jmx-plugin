package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;

public final class MemorythresholdPostGCCount extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemorythresholdPostGCCount", "count",
                "Returns the number of times that the Java virtual machine "
                + "has detected that the memory usage has reached or exceeded "
                + "the collection usage threshold."
        ).addItem(new GraphItemBuilder("TenuredGen")
                .setInfo("ThresholdCount for Tenured Gen")
        ).addItem(new GraphItemBuilder("Eden")
                .setInfo("ThresholdCount for Eden.")
        ).addItem(new GraphItemBuilder("Survivor")
                .setInfo(" ThresholdCount for Survivor."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GetMemoryPoolThresholdCount collector =
                new GetMemoryPoolThresholdCount(connection);
        final String[] temp = collector.GC();

        Output.write("Survivor.value %s", temp[3]);
        Output.write("TenuredGen.value %s", temp[0]);
        Output.write("PermGen.value %s", temp[1]);
        Output.write("Eden.value %s", temp[2]);
    }
}
