package org.munin.plugin.jmx;

import org.nohope.rrdtool.DrawType;
import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;

public final class MemoryEdenUsagePostGC extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemoryEdenUsagePostGC", "bytes",
                "Pool from which memory is initially allocated for most "
                + "objects."
        ).addItem(new GraphItemBuilder("Max")
                .setDraw(DrawType.AREA)
                .setColour("ccff00")
                .setInfo("The maximum amount of memory (in bytes) that can be "
                         + "used for memory management.")
        ).addItem(new GraphItemBuilder("Committed")
                .setDraw(DrawType.LINE2)
                .setColour("0033ff")
                .setInfo("The amount of memory (in bytes) that is guaranteed "
                         + "to be available for use by the Java virtual "
                         + "machine.")
        ).addItem(new GraphItemBuilder("Init")
                .setInfo("The initial amount of memory (in bytes) that the "
                         + "Java virtual machine requests from the operating "
                         + "system for memory management during startup.")
        ).addItem(new GraphItemBuilder("Used")
                .setDraw(DrawType.LINE3)
                .setColour("33cc00")
                .setInfo("The amount of memory currently used (in bytes).")
        ).addItem(new GraphItemBuilder("Threshold")
                .setInfo("The usage threshold value of this memory pool in "
                         + "bytes. The default value is zero.")
        );
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GetCollectionUsage collector =
                new GetCollectionUsage(connection, 3);
        final String[] temp = collector.GC();

        Output.write("Committed.value %s", temp[0]);
        Output.write("Init.value %s", temp[1]);
        Output.write("Max.value %s", temp[2]);
        Output.write("Used.value %s", temp[3]);
        Output.write("Threshold.value %s", temp[4]);
    }
}
