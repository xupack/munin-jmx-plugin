package org.munin.plugin.jmx;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 12/4/11 8:26 PM
 */
public final class Conf {
    private String url;
    private String userName;
    private String password;
    private String category;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }
}
