package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

public final class ClassesLoaded extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ClassesLoaded",
                "classes",
                "The number of classes that are "
                + "currently loaded in the Java virtual machine."
        ).addItem(new GraphItemBuilder("ClassesLoaded"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws IOException {
        final ClassLoadingMXBean classMXBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.CLASS_LOADING_MXBEAN_NAME,
                        ClassLoadingMXBean.class);

        Output.write("ClassesLoaded.value %s", classMXBean.getLoadedClassCount());
    }
}
