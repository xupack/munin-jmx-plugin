package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class CurrentThreadUserTime extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM CurrentThreadUserTime",
                "ns",
                "Returns the CPU time that the current thread has executed in "
                + "user mode in nanoseconds. The returned value is of "
                + "nanoseconds precision but not necessarily nanoseconds "
                + "accuracy."
        ).addItem(new GraphItemBuilder("CurrentThreadUserTime"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final ThreadMXBean threadmxbean = ManagementFactory
                .newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);
        Output.write("CurrentThreadUserTime.value %s",
                threadmxbean.getCurrentThreadUserTime());
    }
}
