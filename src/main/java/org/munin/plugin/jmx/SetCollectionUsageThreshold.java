package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class SetCollectionUsageThreshold {
    private List<MemoryPoolMXBean> gcMBeans;
    private final MBeanServerConnection connection;

    public SetCollectionUsageThreshold(final MBeanServerConnection connection) {
        this.connection = connection;
    }

    public void GC() throws IOException, MalformedObjectNameException {
        final ObjectName gcName = new ObjectName(
                ManagementFactory.MEMORY_POOL_MXBEAN_DOMAIN_TYPE + ",*");
        final Set mBeans = connection.queryNames(gcName, null);
        if (mBeans != null) {
            gcMBeans = new ArrayList<MemoryPoolMXBean>();
            for (final Object mBean : mBeans) {
                final ObjectName objName = (ObjectName) mBean;
                final MemoryPoolMXBean gc =
                        ManagementFactory.newPlatformMXBeanProxy(
                                connection,
                                objName.getCanonicalName(),
                                MemoryPoolMXBean.class);
                gcMBeans.add(gc);
            }
        }


        final long f = 200;
        gcMBeans.get(0).setCollectionUsageThreshold(f);
        gcMBeans.get(3).setCollectionUsageThreshold(f);
        gcMBeans.get(4).setCollectionUsageThreshold(f);
    }
}

