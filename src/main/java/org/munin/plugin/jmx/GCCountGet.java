package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

class GCCountGet {
    private Collection<GarbageCollectorMXBean> gcMBeans;
    private final String[] gcResult = new String[2];
    private final MBeanServerConnection connection;

    public GCCountGet(final MBeanServerConnection connection) {
        this.connection = connection;
    }

    public String[] GC() throws IOException, MalformedObjectNameException {
        final ObjectName gcName  = new ObjectName(
                ManagementFactory.GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE + ",*");


        final Set mBeans = connection.queryNames(gcName, null);
        if (mBeans != null) {
            gcMBeans = new ArrayList<GarbageCollectorMXBean>();
            for (final Object mBean : mBeans) {
                final ObjectName objName = (ObjectName) mBean;
                final GarbageCollectorMXBean gc =
                        ManagementFactory.newPlatformMXBeanProxy(
                                connection,
                                objName.getCanonicalName(),
                                GarbageCollectorMXBean.class);
                gcMBeans.add(gc);
            }
        }


        int i = 0;

        for (final GarbageCollectorMXBean gc : gcMBeans) {
            gcResult[i++] = Long.toString(gc.getCollectionCount());
        }
        return gcResult;
    }
}

