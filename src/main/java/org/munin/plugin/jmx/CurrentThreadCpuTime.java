package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class CurrentThreadCpuTime extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM CurrentThreadCpuTime", "ns",
                "Returns the total CPU time for the current thread in "
                + "nanoseconds. The returned value is of nanoseconds precision "
                + "but not necessarily nanoseconds accuracy. If the "
                + "implementation distinguishes between user mode time and "
                + "system mode time, the returned CPU time is the amount of "
                + "time that the current thread has executed in user mode or "
                + "system mode."
        ).addItem(new GraphItemBuilder("CurrentThreadCpuTime"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws IOException {
        final ThreadMXBean threadmxbean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);

        Output.write("CurrentThreadCpuTime.value %s",
                threadmxbean.getCurrentThreadCpuTime());
    }
}
