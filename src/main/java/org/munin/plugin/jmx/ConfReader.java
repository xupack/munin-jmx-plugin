package org.munin.plugin.jmx;

import javax.management.remote.JMXConnector;
import java.util.HashMap;
import java.util.Map;

final class ConfReader {
    private static final String URL_PARAM = "url";
    private static final String CATEGORY_PARAM = "category";
    private static final String USERNAME_PARAM = "username";
    private static final String PASSWORD_PARAM = "password";

    private static String username;
    private static String password;

    private ConfReader() {
    }

    public static Conf getConnectionInfo() {
        final String url = System.getenv(URL_PARAM);
        String category = System.getenv(CATEGORY_PARAM);
        username = System.getenv(USERNAME_PARAM);
        password = System.getenv(PASSWORD_PARAM);

        if (url == null || "null".equals(url)) {
            Output.write("The following parameters were not configured: url");
            System.exit(1);
        }
        if (category == null || "null".equals(category)) {
            category = "jvm";
        }

        final Conf cfg = new Conf();
        cfg.setCategory(category);
        cfg.setUrl(url);
        cfg.setUserName(username);
        cfg.setPassword(password);
        return cfg;
    }

    public static Map<String, Object> getConnectionCredentials() {
        final Map<String, Object> credentials;
        if (username == null || password == null) {
            return null;
        }

        credentials = new HashMap<String, Object>();
        credentials.put(JMXConnector.CREDENTIALS,
                new String[]{username, password});

        return credentials;
    }
}

