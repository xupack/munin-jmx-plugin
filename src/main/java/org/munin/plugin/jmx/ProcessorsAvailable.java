package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

public final class ProcessorsAvailable extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ProcessorsAvailable", "processors",
                "Returns the number of processors available to the Java "
                + "virtual machine. This value may change during a "
                + "particular invocation of the virtual machine."
        ).addItem(new GraphItemBuilder("ProcessorsAvailable"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final OperatingSystemMXBean osMXBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME,
                        OperatingSystemMXBean.class);

        Output.write("ProcessorsAvailable.value %s",
                osMXBean.getAvailableProcessors());
    }
}
