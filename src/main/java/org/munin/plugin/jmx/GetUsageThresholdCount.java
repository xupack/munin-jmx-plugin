package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class GetUsageThresholdCount {
    private List<MemoryPoolMXBean> gcMBeans;
    private final String[] gcResult = new String[2];
    private final MBeanServerConnection connection;

    public GetUsageThresholdCount(final MBeanServerConnection connection) {
        this.connection = connection;
    }

    public String[] GC() throws IOException, MalformedObjectNameException {
        final ObjectName gcName = new ObjectName(
                ManagementFactory.MEMORY_POOL_MXBEAN_DOMAIN_TYPE + ",*");

        final Set mBeans = connection.queryNames(gcName, null);
        if (mBeans != null) {
            gcMBeans = new ArrayList<MemoryPoolMXBean>();
            for (final Object mBean : mBeans) {
                final ObjectName objName = (ObjectName) mBean;
                final MemoryPoolMXBean gc =
                        ManagementFactory.newPlatformMXBeanProxy(
                                connection,
                                objName.getCanonicalName(),
                                MemoryPoolMXBean.class);
                gcMBeans.add(gc);
            }
        }

        try {
            gcResult[0] = Long.toString(gcMBeans.get(0).getUsageThresholdCount());
        } catch (UnsupportedOperationException u) {
            gcResult[0] = "U";
        }

        try {
            gcResult[1] = Long.toString(gcMBeans.get(1).getUsageThresholdCount());
        } catch (UnsupportedOperationException u) {
            gcResult[1] = "U";
        }

        return gcResult;
    }
}

