package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class Threads extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM Threads", "threads",
                "Returns the current number of live threads including "
                + "both daemon and non-daemon threads."
        ).addItem(new GraphItemBuilder("Threads"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final ThreadMXBean threadmxbean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);

        Output.write("Threads.value %s", threadmxbean.getThreadCount());
    }
}
