package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

public final class ClassesLoadedTotal extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ClassesLoadedTotal",
                "classes",
                " The total number of classes that "
                + "have been loaded since the Java virtual machine "
                + "has started execution."
        ).addItem(new GraphItemBuilder("ClassesLoadedTotal"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws IOException {
        final ClassLoadingMXBean classMXBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.CLASS_LOADING_MXBEAN_NAME,
                        ClassLoadingMXBean.class);
        Output.write("ClassesLoadedTotal.value %s",
                classMXBean.getTotalLoadedClassCount());
    }
}
