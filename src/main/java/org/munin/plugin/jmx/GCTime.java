package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;
import org.nohope.rrdtool.Type;

import javax.management.MBeanServerConnection;

public final class GCTime extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM GarbageCollectionTime", "ms",
                "The Sun JVM defines garbage collection in two modes: Minor "
                + "copy collections and Major Mark-Sweep-Compact collections. "
                + "A minor collection runs relatively quickly and involves "
                + "moving live data around the heap in the presence of "
                + "running threads. A major collection is a much more "
                + "intrusive garbage collection that suspends all execution "
                + "threads while it completes its task. In terms of "
                + "performance tuning the heap, the primary goal is to reduce "
                + "the frequency and duration of major garbage collections."
        ).addItem(new GraphItemBuilder("CopyTime")
                .setLabel("MinorTime")
                .setType(Type.DERIVE)
                .setInfo("The approximate accumulated collection elapsed time "
                         + "in milliseconds. This method returns -1 if the "
                         + "collection elapsed time is undefined for this "
                         + "collector.")
        ).addItem(new GraphItemBuilder("MarkSweepCompactTime")
                .setLabel("MajorTime")
                .setType(Type.DERIVE)
                .setInfo("The approximate accumulated collection elapsed time "
                         + "in milliseconds. This method returns -1 if the "
                         + "collection elapsed time is undefined for this "
                         + "collector.The Java virtual machine implementation "
                         + "may use a high resolution timer to measure the "
                         + "elapsed time. This method may return the same "
                         + "value even if the collection count has been "
                         + "incremented if the collection elapsed time is "
                         + "very short."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GCTimeGet collector = new GCTimeGet(connection);
        final String[] temp = collector.GC();

        Output.write("CopyTime.value " + temp[0]);
        Output.write("MarkSweepCompactTime.value " + temp[1]);
    }
}

