package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.Map;

/* Inherit from this if you need another method for jboss/glassfish/etc */

class BasicMBeanConnection {
    private BasicMBeanConnection() {
    }

    public static MBeanServerConnection get() throws IOException {
        final Conf conn = ConfReader.getConnectionInfo();

        final JMXServiceURL u = new JMXServiceURL(conn.getUrl());
        final Map<String, Object> credentials = ConfReader.getConnectionCredentials();
        final JMXConnector c = JMXConnectorFactory.connect(u, credentials);
        return c.getMBeanServerConnection();
    }
}

