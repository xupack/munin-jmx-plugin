package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class ThreadsStartedTotal extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ThreadsStartedTotal", "threads",
                "Returns the total number of threads created and also started "
                + "since the Java virtual machine started."
        ).addItem(new GraphItemBuilder("ThreadsStartedTotal"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final ThreadMXBean threadmxbean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);

        Output.write("ThreadsStartedTotal.value %s",
                threadmxbean.getTotalStartedThreadCount());
    }
}
