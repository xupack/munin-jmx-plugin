package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

public final class MemoryObjectsPending extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemoryObjectsPending", "objects", null
        ).addItem(new GraphItemBuilder("Objects")
                .setInfo("The approximate number of objects for which "
                         + "finalization is pending."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final MemoryMXBean mBean =
                ManagementFactory.newPlatformMXBeanProxy(connection,
                        ManagementFactory.MEMORY_MXBEAN_NAME,
                        MemoryMXBean.class);

        Output.write("Objects.value %s",
                mBean.getObjectPendingFinalizationCount());
    }
}
