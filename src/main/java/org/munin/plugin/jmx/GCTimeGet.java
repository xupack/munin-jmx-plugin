package org.munin.plugin.jmx;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

class GCTimeGet {
    private Collection<GarbageCollectorMXBean> gcMBeans;
    private final String[] gcResult = new String[2];
    private final MBeanServerConnection connection;

    public GCTimeGet(final MBeanServerConnection connection) {
        this.connection = connection;
    }

    public String[] GC() throws IOException, MalformedObjectNameException {
        final ObjectName gcName = new ObjectName(
                ManagementFactory.GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE + ",*");

        final Set<ObjectName> mBeans = connection.queryNames(gcName, null);
        if (mBeans != null) {
            gcMBeans = new ArrayList<GarbageCollectorMXBean>();
            for (final ObjectName objName : mBeans) {
                final GarbageCollectorMXBean gc =
                        ManagementFactory.newPlatformMXBeanProxy(connection,
                                objName.getCanonicalName(),
                                GarbageCollectorMXBean.class);
                gcMBeans.add(gc);
            }
        }

        int i = 0;
        for (final GarbageCollectorMXBean gc : gcMBeans) {
            gcResult[i++] = formatMillis(gc.getCollectionTime());
        }

        return gcResult;
    }

    private String formatMillis(final double ms) {
        return String.format("%.4f", ms / 1000);
    }
}

