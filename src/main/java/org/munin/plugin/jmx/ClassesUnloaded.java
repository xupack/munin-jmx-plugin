package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

public final class ClassesUnloaded extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ClassesUnloaded",
                "classes",
                "The total number of classes unloaded since the "
                + "Java virtual machine has started execution."
        ).addItem(new GraphItemBuilder("UnloadedClass"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws IOException {
        final ClassLoadingMXBean classMXBean =
                ManagementFactory.newPlatformMXBeanProxy(connection,
                        ManagementFactory.CLASS_LOADING_MXBEAN_NAME,
                        ClassLoadingMXBean.class);
        Output.write("UnloadedClass.value %s",
                classMXBean.getUnloadedClassCount());
    }
}
