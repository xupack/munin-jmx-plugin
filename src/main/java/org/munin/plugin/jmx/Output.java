package org.munin.plugin.jmx;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/14/12 4:54 PM
 */
final class Output {
    private Output() {
    }

    public static void write(final String format, final Object... args) {
        System.out.println(String.format(format, args));
    }
}
