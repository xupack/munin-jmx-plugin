package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;

import javax.management.MBeanServerConnection;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/15/12 3:11 PM
 */
public abstract class EntryPoint {
    protected abstract GraphBuilder getGraphBuilder();
    protected abstract void calculate(MBeanServerConnection connection)
            throws Exception;

    public static void main(final String... args) {
        final Conf c = ConfReader.getConnectionInfo();
        if (args.length == 2) {
            final String className = "org.munin.plugin.jmx." + args[0];
            final EntryPoint point;
            try {
                point = (EntryPoint) Class.forName(className).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if ("config".equals(args[1])) {
                Output.write(point.getGraphBuilder().build().asString(c));
            } else {
                try {
                    final MBeanServerConnection connection = BasicMBeanConnection.get();
                    point.calculate(connection);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
