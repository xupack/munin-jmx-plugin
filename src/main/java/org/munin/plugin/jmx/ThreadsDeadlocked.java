package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public final class ThreadsDeadlocked extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM ThreadsDeadlocked", "threads",
                "Returns the number of deadlocked threads for the JVM. "
                + "Usually not available at readonly access level."
        ).addItem(new GraphItemBuilder("ThreadsDeadlocked"));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final ThreadMXBean mxBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.THREAD_MXBEAN_NAME,
                        ThreadMXBean.class);

        Output.write("ThreadsDeadlocked.value %s",
                mxBean.findMonitorDeadlockedThreads() == null
                        ? 0
                        : mxBean.findMonitorDeadlockedThreads().length);
    }
}

