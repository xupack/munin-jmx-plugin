package org.munin.plugin.jmx;
/**
 *
 * @author Diyar
 */

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.CompilationMXBean;
import java.lang.management.ManagementFactory;

public final class CompilationTimeTotal extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM CompilationTimeTotal", "ms",
                "value does not indicate the level of performance of the Java "
                + "virtual machine and is not intended for performance "
                + "comparisons of different virtual machine implementations. "
                + "The implementations may have different definitions and "
                + "different measurements of the compilation time."
        ).addItem(new GraphItemBuilder("CompilationTimeTotal")
                .setInfo("The approximate accumulated elapsed time "
                         + "(in milliseconds) spent in compilation. If "
                         + "multiple threads are used for compilation, this "
                         + "value is summation of the approximate time that "
                         + "each thread spent in compilation."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws IOException {
        final CompilationMXBean osMXBean =
                ManagementFactory.newPlatformMXBeanProxy(
                        connection,
                        ManagementFactory.COMPILATION_MXBEAN_NAME,
                        CompilationMXBean.class);

        Output.write("CompilationTimeTotal.value %s",
                osMXBean.getTotalCompilationTime());
    }
}
