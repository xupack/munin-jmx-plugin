package org.munin.plugin.jmx;

import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;
import org.nohope.rrdtool.Type;

import javax.management.MBeanServerConnection;

public final class GCCount extends EntryPoint {
    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM GarbageCollectionCount", "count",
                "The Sun JVM defines garbage collection in two modes: Minor "
                + "copy collections and Major Mark-Sweep-Compact collections. "
                + "A minor collection runs relatively quickly and involves "
                + "moving live data around the heap in the presence of "
                + "running threads. A major collection is a much more "
                + "intrusive garbage collection that suspends all execution "
                + "threads while it completes its task. In terms of "
                + "performance tuning the heap, the primary goal is to "
                + "reduce the frequency and duration of major "
                + "garbage collections."
        ).addItem(new GraphItemBuilder("CopyCount")
                .setLabel("MinorCount")
                .setType(Type.DERIVE)
                .setMin(0)
                .setInfo("The total number of collections that have occurred. "
                         + "This method returns -1 if the collection count is "
                         + "undefined for this collector.")

        ).addItem(new GraphItemBuilder("MarkSweepCompactCount")
                .setLabel("MajorCount")
                .setType(Type.DERIVE)
                .setMin(0)
                .setInfo("The total number of collections that have occurred."
                         + " This method returns -1 if the collection count "
                         + "is undefined for this collector."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GCCountGet collector = new GCCountGet(connection);
        final String[] temp = collector.GC();

        Output.write("CopyCount.value %s", temp[0]);
        Output.write("MarkSweepCompactCount.value %s", temp[1]);
    }
}
