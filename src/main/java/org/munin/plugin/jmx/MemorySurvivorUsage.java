package org.munin.plugin.jmx;

import org.nohope.rrdtool.DrawType;
import org.nohope.rrdtool.GraphBuilder;
import org.nohope.rrdtool.GraphItemBuilder;

import javax.management.MBeanServerConnection;

public final class MemorySurvivorUsage extends EntryPoint {

    @Override
    protected GraphBuilder getGraphBuilder() {
        return new GraphBuilder("JVM MemorySurvivorUsage", "bytes",
                "Returns an estimate of the memory usage of this memory pool."
        ).addItem(new GraphItemBuilder("Max")
                .setInfo("The maximum amount of memory (in bytes) that "
                         + "can be used for memory management.")
                .setDraw(DrawType.AREA)
                .setColour("ccff00")
        ).addItem(new GraphItemBuilder("Committed")
                .setInfo("The amount of memory (in bytes) that is "
                         + "guaranteed to be available for use by the "
                         + "Java virtual machine.")
                .setDraw(DrawType.LINE2)
                .setColour("0033ff")
        ).addItem(new GraphItemBuilder("Init")
                .setInfo("The initial amount of memory (in bytes) "
                         + "that the Java virtual machine requests "
                         + "from the operating system for memory "
                         + "management during startup.")
        ).addItem(new GraphItemBuilder("Used")
                .setInfo("The amount of memory currently used "
                         + "(in bytes).")
                .setDraw(DrawType.LINE3)
                .setColour("33cc00")
        ).addItem(new GraphItemBuilder("Threshold")
                .setInfo("The usage threshold value of this memory "
                         + "pool in bytes."));
    }

    @Override
    protected void calculate(final MBeanServerConnection connection)
            throws Exception {
        final GetUsage collector = new GetUsage(connection, 4);
        final String[] temp = collector.GC();

        System.out.println("Committed.value " + temp[0]);
        System.out.println("Init.value " + temp[1]);
        System.out.println("Max.value " + temp[2]);
        System.out.println("Used.value " + temp[3]);
        System.out.println("Threshold.value " + temp[4]);
    }
}
