package org.nohope.rrdtool;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/15/12 2:53 PM
 */
public class GraphBuilder {
    private final Graph graph;
    public GraphBuilder(final String title,
                        final String verticalLabel,
                        final String info) {
        graph = new Graph(title, verticalLabel, info);
    }

    public GraphBuilder addItem(final GraphItemBuilder builder) {
        graph.addItem(builder.build());
        return this;
    }

    public GraphBuilder setArgs(final String args) {
        graph.setArgs(args);
        return this;
    }

    public Graph build() {
        return graph;
    }
}
