package org.nohope.rrdtool;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/15/12 2:53 PM
 */
public class GraphItemBuilder {
    private final GraphItem item;
    public GraphItemBuilder(final String name) {
        item = new GraphItem(name);
    }

    public GraphItemBuilder setLabel(final String label) {
        item.setLabel(label);
        return this;
    }

    public GraphItemBuilder setDraw(final DrawType type) {
        item.setDraw(type);
        return this;
    }

    public GraphItemBuilder setColour(final String colour) {
        item.setColour(colour);
        return this;
    }

    public GraphItemBuilder setInfo(final String info) {
        item.setInfo(info);
        return this;
    }

    public GraphItemBuilder setType(final Type type) {
        item.setType(type);
        return this;
    }

    public GraphItemBuilder setMin(final Integer min) {
        item.setMin(min);
        return this;
    }

    public GraphItem build() {
        return item;
    }
}
