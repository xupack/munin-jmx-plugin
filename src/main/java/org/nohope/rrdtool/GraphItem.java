package org.nohope.rrdtool;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/14/12 5:02 PM
 */
public class GraphItem {
    private final String name;
    private String label = null;
    private String info = null;
    private DrawType draw = null;
    private String colour = null;
    private Integer min = null;
    private Type type = null;

    public GraphItem(final String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(final String info) {
        this.info = info;
    }

    public DrawType getDraw() {
        return draw;
    }

    public void setDraw(final DrawType draw) {
        this.draw = draw;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(final String colour) {
        this.colour = colour;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(final Integer min) {
        this.min = min;
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    private StringBuilder set(final StringBuilder builder,
                              final String key,
                              final Object val) {
        if (val != null) {
            builder.append(name)
                   .append('.')
                   .append(key)
                   .append(' ')
                   .append(val)
                   .append('\n');
        }

        return builder;
    }

    public String asString() {
        final StringBuilder builder = new StringBuilder();

        set(builder, "label", label == null ? name : label);
        set(builder, "info", info);
        set(builder, "draw", draw);
        set(builder, "colour", colour);
        set(builder, "type", type);
        set(builder, "min", min);



        return builder.toString();
    }
}
