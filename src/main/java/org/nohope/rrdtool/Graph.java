package org.nohope.rrdtool;

import org.munin.plugin.jmx.Conf;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/14/12 5:02 PM
 */
public class Graph {
    private final String title;
    private final String verticalLabel;
    private final String info;
    private String args = null;
    private final List<GraphItem> items = new ArrayList<GraphItem>();

    public Graph(final String title,
                 final String verticalLabel,
                 final String info) {
        this.info = info;
        this.verticalLabel = verticalLabel;
        this.title = title;
    }

    public Graph addItem(final GraphItem item) {
        items.add(item);
        return this;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(final String args) {
        this.args = args;
    }

    private static StringBuilder set(final StringBuilder builder,
                                     final String key,
                                     final Object val) {

        if (val != null) {
            builder.append(key).append(' ').append(val).append('\n');
        }

        return builder;
    }

    public String asString(final Conf c) {
        final StringBuilder builder = new StringBuilder();
        set(builder, "graph_title", title);
        set(builder, "graph_args", args);
        set(builder, "graph_vlabel", verticalLabel);
        set(builder, "graph_category", c.getCategory());
        set(builder, "graph_info", info);

        for (final GraphItem item : items) {
            builder.append(item.asString());
        }

        return builder.toString();
    }
}
