package org.nohope.rrdtool;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 3/14/12 5:44 PM
 */
public enum DrawType {
    AREA
    , LINE2
    , LINE3
}
